import java.awt.print.Printable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {
    private LinkedHashMap<String, String> menu;
    private Scanner in;
    private Locale locale;
    private ResourceBundle bundle;

    public Menu(){
        in = new Scanner(System.in);
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);

        setMenu();
    }

    private void setMenu(){
        menu = new LinkedHashMap<String, String>();

        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void stringUtils(){
        StringUtils utils = new StringUtils();

        utils.addToParameters(10).addToParameters("14/2018").addToParameters("Hunko");
        System.out.println(utils.concat());
    }

    private void regEx(){
        String string = "London is the capital of Great Britain.";
        String regex = "^[A-Z][A-Za-z,;'\"\\s]+[.?!]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);

        System.out.println(string + " - " + matcher.matches());
        System.out.println(string.toLowerCase() + " - " + pattern.matcher(string.toLowerCase()).matches());
    }

    private void ukMenu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void enMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void frMenu(){
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void jaMenu(){
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void printMenu(){
        System.out.println();
        for(String line : menu.keySet())
            System.out.println(menu.get(line));
        System.out.println();
    }

    public void run(){
        String choice;

        do{
            printMenu();
            System.out.println("Select menu point:");
            choice = in.nextLine().toUpperCase();
            switch(choice.charAt(0)){
                case '1':
                    stringUtils();
                    break;
                case '2':
                    regEx();
                    break;
                case '3':
                    ukMenu();
                    break;
                case '4':
                    enMenu();
                    break;
                case '5':
                    frMenu();
                    break;
                case '6':
                    jaMenu();
                    break;
            }
        }while(!choice.equals("Q"));
    }
}
